/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.torrent.search.engine;

import com.j.web.eztorrent.entity.Category;
import com.j.web.eztorrent.entity.MovieInfo;
import com.j.web.eztorrent.entity.ResultInfo;
import com.j.web.eztorrent.helper.CommonUtils;
import com.torrent.search.engine.constants.TorrentUrl;
import com.j.web.eztorrent.helper.FileUtils;
import com.j.web.eztorrent.helper.HttpUtils;
import java.util.ArrayList;
import java.util.List;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 *
 * @author dotrungduchd
 */
public class RARBGSearchEngine implements TorrentSearchEngine {

    @Override
    public List<ResultInfo> search(String query) {

        List<ResultInfo> list = new ArrayList<>();
        long t1 = System.currentTimeMillis();

        try {

            String url = String.format("%s%s%s",
                    TorrentUrl.RARBG,
                    "/torrents.php?search=",
                    query.replace(" ", "+"));

            // System.out.println("url = " + url);
            Document doc = HttpUtils.getDocumentByJsoup(url);

            // System.out.println("location = " + doc.location());
            if (doc.location().contains("defence")) {
                String html = doc.html();
                FileUtils.write("rarbg.html", html);
                return list;
            }

            Elements rows = doc.getElementsByClass("lista2");

            for (Element row : rows) {

                //<editor-fold defaultstate="collapsed" desc="comment">
                // query, timestamp
                long timestamp = System.currentTimeMillis();

                if (row.getElementsByTag("td").size() < 6) {
                    continue;
                }

                // category
                Element category2 = row.getElementsByTag("td").get(1)
                        .select("span[style=color:DarkSlateGray]").first();
                String category2Str = "";
                if (category2 != null) {
                    // System.out.println("category = " + category2.text());
                    category2Str = category2.text();
                }

                // date
                Element dateFilm = row.getElementsByTag("td").get(2);
                String date = "";
                if (dateFilm != null) {
                    // System.out.println("date = " + dateFilm.text());
                    date = dateFilm.text();
                }

                // size
                Element sizeFilm = row.getElementsByTag("td").get(3);
                String size = "";
                if (sizeFilm != null) {
                    // System.out.println("size = " + sizeFilm.text());
                    size = sizeFilm.text();
                }

                // seeder
                Element seederFilm = row.getElementsByTag("td").get(4);
                int seeder = 0;
                if (seederFilm != null) {
                    seeder = CommonUtils.tryParse(seederFilm.text());
                }

                // leecher
                Element leecherFilm = row.getElementsByTag("td").get(5);
                int leecher = 0;
                if (leecherFilm != null) {
                    leecher = CommonUtils.tryParse(leecherFilm.text());
                }

//</editor-fold>
                // link
                Elements links = row.getElementsByTag("a");
                for (Element link : links) {
                    String linkHref = link.attr("href");
                    // System.out.println("link = " + linkHref);

                    if (linkHref.contains("/torrent/")) {

                        //<editor-fold defaultstate="collapsed" desc="torrent film">
                        ResultInfo resultInfo = new ResultInfo();
                        resultInfo.setQuery(query);
                        resultInfo.setTimestamp(timestamp);
                        resultInfo.setCategory1(Category.getByTag(category2Str).val());
                        resultInfo.setCategory2(category2Str);
                        resultInfo.setDate(date);
                        resultInfo.setSize(size);
                        resultInfo.setSeeds(seeder);
                        resultInfo.setLeeches(leecher);

                        // name
                        String name = link.text();
                        resultInfo.setName(name);
                        // System.out.println("title = " + title);

                        String urlFilm = TorrentUrl.RARBG + linkHref;
                        Document docFilm = HttpUtils.getDocumentByJsoup(urlFilm);

                        // detail info
                        parseFilmDetail(docFilm, resultInfo);

                        // query time
                        resultInfo.setQueryTime(System.currentTimeMillis());
                        list.add(resultInfo);
//</editor-fold>
                        break;

                    }

                    // imdb info
                    if (linkHref.contains("/torrents.php?imdb=")) {

                        String urlImdb = TorrentUrl.RARBG + linkHref;
                        // System.out.println("urlImdb = " + urlImdb);
                    }

                }

            }

        } catch (Exception ex) {
            System.out.println(String.format("ex: %s\n %s ", ex.toString(), ex.getStackTrace()));
        }

        long t2 = System.currentTimeMillis();
        // System.out.println("process time = " + (t2 - t1));

        return list;
    }

    private void parseFilmDetail(Document docFilm, ResultInfo resultInfo) {

        // name
        if ("".equals(resultInfo.getName())) {
            Elements tableFilmName = docFilm.getElementsByClass("lista-rounded");
            Element filmName = tableFilmName.first();
            resultInfo.setName(filmName.getElementsByTag("h1").first().html());
            // System.out.println("name = " + resultInfo.name);
        }

        // url magnet
        MovieInfo movieInfo = new MovieInfo();
        Elements linkFilms = docFilm.getElementsByTag("a");
        if (linkFilms != null && !linkFilms.isEmpty()) {

            int count = 0;
            for (Element linkFilm : linkFilms) {
                String linkFilmHref = linkFilm.attr("href");

                if (linkFilmHref.contains("magnet:?")) {
                    resultInfo.setUrl(linkFilmHref);
                    // System.out.println("url = " + resultInfo.url);
                    count++;
                }

                if (linkFilmHref.contains("http://imdb.com/title/")) {
                    movieInfo.imdbID = linkFilmHref
                            .replace("http://imdb.com/title/", "")
                            .replace("/", "");
                    count++;
                }

                if (count == 2) {
                    break;
                }
            }
        }

        Elements rowTableFilms = docFilm.getElementsByTag("tr");
        for (Element rowTableFilm : rowTableFilms) {

            Elements tds = rowTableFilm.getElementsByClass("header2");

            for (Element td : tds) {

                // category
                if (resultInfo.getCategory1().equals(Category.OTHERS.val())) {
                    if (td.text().equals("Category:")) {

                        String categoryTags = td.nextElementSibling().text();
                        resultInfo.setCategory1(Category.getByTag(categoryTags).val());
//                    resultInfo.setCategory1(td.nextElementSibling().text());
                        // System.out.println("category = " + resultInfo.category1);
                        break;
                    }
                }

                // seeder and leecher
                if (resultInfo.getSeeds() == 0 && resultInfo.getLeeches() == 0) {
                    if (td.text().equals("Peers:")) {
                        //<editor-fold defaultstate="collapsed" desc="seeder and leeches">
                        String peerString = td.nextElementSibling().text();
                        // System.out.println("Peers = " + peerString);
                        if (peerString != null) {
                            peerString = peerString.replace(" ", "");
                            int seederBeginIndex = peerString.indexOf("Seeders:") + 8;
                            int seederEndIndex = peerString.indexOf(",");
                            int leecherBeginIndex = peerString.indexOf("Leechers:") + 9;
                            int leecherEndIndex = peerString.indexOf("=");
                            if (seederBeginIndex > 0 && seederEndIndex > 0
                                    && seederBeginIndex < seederEndIndex) {
                                resultInfo.setSeeds(Integer.parseInt(
                                        peerString.substring(
                                                seederBeginIndex,
                                                seederEndIndex)));
                                // System.out.println("Seeder = " + resultInfo.seeds);
                            }

                            if (leecherBeginIndex > 0 && leecherEndIndex > 0
                                    && leecherBeginIndex < leecherEndIndex) {
                                resultInfo.setLeeches(Integer.parseInt(
                                        peerString.substring(
                                                leecherBeginIndex,
                                                leecherEndIndex)));
                                // System.out.println("Leeches = " + resultInfo.leeches);
                            }
                        }
//</editor-fold>
                        break;
                    }
                }

                // movie Title
                if (td.text().equals("Title:")) {
                    movieInfo.Title = td.nextElementSibling().text();
                    // System.out.println("movie Name = " + movieInfo.Name);
                    break;
                }

                // movie Year
                if (td.text().equals("Year:")) {
                    movieInfo.Year = td.nextElementSibling().text();
                    // System.out.println("movie Year = " + movieInfo.Year);
                    break;
                }

                // movie Runtime
                if (td.text().equals("Runtime:")) {
                    movieInfo.Runtime = td.nextElementSibling().text();
                    // System.out.println("movie Runtime = " + movieInfo.Runtime);
                    break;
                }

                // movie Genres
                if (td.text().equals("Genres:")) {
                    movieInfo.Genre = td.nextElementSibling().text();
                    // System.out.println("movie Genre = " + movieInfo.Genre);
                    break;
                }

                // movie Director
                if (td.text().equals("Director:")) {
                    movieInfo.Director = td.nextElementSibling().text();
                    // System.out.println("movie Director = " + movieInfo.Director);
                    break;
                }

                // movie Actors
                if (td.text().equals("Actors:")) {
                    movieInfo.Actors = td.nextElementSibling().text();
                    // System.out.println("movie Actors = " + movieInfo.Actors);
                    break;
                }

                // movie Plot
                if (td.text().equals("Plot:")) {
                    movieInfo.Plot = td.nextElementSibling().text();
                    // System.out.println("movie Plot = " + movieInfo.Plot);
                    break;
                }

                // movie Poster
                if (td.text().equals("Poster:")) {
                    if (td.nextElementSibling().getElementsByTag("img") != null) {
                        movieInfo.Poster = td.nextElementSibling().getElementsByTag("img").attr("src");
                        // System.out.println("movie Poster = " + movieInfo.Poster);
                    }
                    break;
                }

                // movie imdb
                if (td.text().equals("IMDB Rating:")) {
                    String imdbString = td.nextElementSibling().text();
                    imdbString = imdbString.replace(" ", "");
                    // System.out.println(imdbString);

                    int ratingBeginIndex = 0;
                    int ratingEndIndex = imdbString.indexOf("from");
                    int voteBeginIndex = ratingEndIndex + 4;
                    int voteEndIndex = imdbString.indexOf("users");
                    if (ratingBeginIndex >= 0 && ratingEndIndex > 0
                            && ratingBeginIndex < ratingEndIndex) {
                        movieInfo.imdbRating = imdbString.substring(ratingBeginIndex, ratingEndIndex);
                    }
                    if (voteBeginIndex > 0 && voteEndIndex > 0
                            && voteBeginIndex < voteEndIndex) {
                        movieInfo.imdbVotes = imdbString.substring(voteBeginIndex, voteEndIndex);
                    }

                    // System.out.println("movie imdbRating = " + movieInfo.imdbRating);
                    // System.out.println("movie imdbVotes = " + movieInfo.imdbVotes);
                    break;
                }

            }

        }
    }

}
