/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.torrent.search.engine;

import com.j.web.eztorrent.entity.ResultInfo;
import com.torrent.search.engine.constants.TorrentSource;
import java.util.List;

/**
 *
 * @author dotrungduchd
 */
public class Main {

    public static void main(String[] args) {

//        test();

        List<ResultInfo> listFilm = new TorrentSearch().search("love", TorrentSource.RARBG);
//        List<ResultInfo> listFilm = new TorrentSearch().search("batman", TorrentSource.RARBG);

        int i = 0;
        for (ResultInfo resultInfo : listFilm) {
            System.out.println(String.format("index %s resultInfo = %s", i++, resultInfo.toString()));
        }
    }

    public static void test() {
        String imdbString = "7.6/10 from 280979 users Updated : 2017-04-28";
        imdbString = imdbString.replace(" ", "");
        System.out.println(imdbString);

        int ratingBeginIndex = 0;
        int ratingEndIndex = imdbString.indexOf("from");
        int voteBeginIndex = ratingEndIndex + 4;
        int voteEndIndex = imdbString.indexOf("users");
        if (ratingBeginIndex >= 0 && ratingEndIndex > 0 
               && ratingBeginIndex < ratingEndIndex) {
            String rating = imdbString.substring(ratingBeginIndex, ratingEndIndex);
        }
        if (voteBeginIndex > 0 && voteEndIndex > 0 
               && voteBeginIndex < voteEndIndex) {
            String vote = imdbString.substring(voteBeginIndex, voteEndIndex);
        }
        
        System.out.println(" ");
    }
}
