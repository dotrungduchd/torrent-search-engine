package com.torrent.search.engine;

import com.j.web.eztorrent.entity.ResultInfo;
import java.util.List;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author dotrungduchd
 */
public interface TorrentSearchEngine {
    
    public List<ResultInfo> search(String query);
}
