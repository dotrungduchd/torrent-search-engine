/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.torrent.search.engine;

import com.j.web.eztorrent.entity.ResultInfo;
import com.torrent.search.factory.TorrentSearchEngineFactory;
import java.util.List;

/**
 *
 * @author dotrungduchd
 */
public class TorrentSearch {

    public List<ResultInfo> search(String query, int source) {

        TorrentSearchEngine engine = TorrentSearchEngineFactory.get(source);
        List<ResultInfo> list = engine.search(query);

        return list;
    }

}
