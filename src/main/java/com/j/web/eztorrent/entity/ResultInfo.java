package com.j.web.eztorrent.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.j.web.eztorrent.helper.JsonUtil;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.index.TextIndexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "result-info")
@CompoundIndexes({
    @CompoundIndex(name = "source_query", def = "{'source' : 1, 'query': 1}")})
public class ResultInfo {

    @Id
    private String id;
    private String name;
    private String size;
    private String category1;
    private String category2;
    private String url;
    private boolean vip;
    private int seeds;
    private int leeches;
    private String date;
    @TextIndexed
    private String textIndex;

    // search info
    @JsonIgnore
    private int source;
    @JsonIgnore
    private String query;

    //monitor
    @JsonIgnore
    private long timestamp = System.currentTimeMillis();
    @JsonIgnore
    private long queryTime;

    //tracking
    private long download;
    private long stream;

    // movie info
    private MovieInfo movieInfo;

    // torrent files
    private TorrentMetaInfo metaInfo;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public boolean isVip() {
        return vip;
    }

    public void setVip(boolean vip) {
        this.vip = vip;
    }

    public int getSeeds() {
        return seeds;
    }

    public void setSeeds(int seeds) {
        this.seeds = seeds;
    }

    public int getLeeches() {
        return leeches;
    }

    public void setLeeches(int leeches) {
        this.leeches = leeches;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public long getQueryTime() {
        return queryTime;
    }

    public void setQueryTime(long queryTime) {
        this.queryTime = queryTime;
    }

    public String getId() {
        return id;
    }

    public void setId() {
        this.id = generateId();
    }

    public int getSource() {
        return source;
    }

    public void setSource(int source) {
        this.source = source;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    private String generateId() {
        return MagnetHelper.getHash(url);
    }

    public String getCategory1() {
        return category1;
    }

    public void setCategory1(String category1) {
        this.category1 = category1;
    }

    public String getCategory2() {
        return category2;
    }

    public void setCategory2(String category2) {
        this.category2 = category2;
    }

    public long getDownload() {
        return download;
    }

    public void setDownload(long download) {
        this.download = download;
    }

    public long getStream() {
        return stream;
    }

    public void setStream(long stream) {
        this.stream = stream;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public MovieInfo getMovieInfo() {
        return movieInfo;
    }

    public void setMovieInfo(MovieInfo movieInfo) {
        this.movieInfo = movieInfo;
    }

    public TorrentMetaInfo getMetaInfo() {
        return metaInfo;
    }

    public void setMetaInfo(TorrentMetaInfo metaInfo) {
        this.metaInfo = metaInfo;
    }

    public String getTextIndex() {
        return textIndex;
    }

    public void setTextIndex(String textIndex) {
        this.textIndex = textIndex;
    }

    @Override
    public String toString() {
        JsonUtil jsonUtil = new JsonUtil();
        return jsonUtil.serialize(this);
    }

}
