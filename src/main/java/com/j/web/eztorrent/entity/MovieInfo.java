package com.j.web.eztorrent.entity;


import com.j.web.eztorrent.helper.JsonUtil;
import java.util.List;

/**
 * Created by lavalamp on 28/04/2017.
 */
public class MovieInfo {
    public String Title;
    public String Year;
    public String Rated;
    public String Released;
    public String Runtime;
    public String Genre;
    public String Director;
    public String Writer;
    public String Actors;
    public String Plot;
    public String Language;
    public String Country;
    public String Awards;
    public String Poster;
    public List<Ratings> Ratings;
    public String Metascore;
    public String imdbRating;
    public String imdbVotes;
    public String imdbID;
    public String Type;
    public String DVD;
    public String BoxOffice;
    public String Production;
    public String Website;

    @Override
    public String toString() {
        JsonUtil jsonUtil = new JsonUtil();
        return jsonUtil.serialize(this);
    }
}
