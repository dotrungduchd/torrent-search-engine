package com.j.web.eztorrent.entity;

import java.util.Arrays;

/**
 * Created by lavalamp on 30/03/2017.
 */
public enum Category {
    VIDEO("Video"),
    AUDIO("Audio"),
    NOTALLOW("NotAllow"),
    OTHERS("Others");

    private String val;

    private Category(String val) {
        this.val = val;
    }

    private static String[] VIDEO_TAG = {"video", "movie"};
    private static String[] AUDIO_TAG = {"audio", "music"};
    private static String[] NOTALLOW_TAG = {"tits", "nude", "butts", "xxx", "18+"};

    public static Category find(String val) {
        return Arrays.stream(Category.values())
                .filter(source -> source.val.equals(val)).findFirst().orElse(OTHERS);
    }

    public static Category get(String categoryString) {

        String catString = categoryString.toLowerCase();
        for (Category cat : Category.values()) {

            if (catString.contains(cat.val().toLowerCase())) {
                return cat;
            }
        }

        return OTHERS;
    }

    public static Category getByTag(String tagsString) {

        String tags = tagsString.toLowerCase();

        for (String video : VIDEO_TAG) {
            if (tags.contains(video)) {
                return VIDEO;
            }
        }

        for (String audio : AUDIO_TAG) {
            if (tags.contains(audio)) {
                return AUDIO;
            }
        }

        for (String nowallow : NOTALLOW_TAG) {
            if (tags.contains(nowallow)) {
                return NOTALLOW;
            }
        }

        return OTHERS;
    }

    public String val() {
        return val;
    }

}
