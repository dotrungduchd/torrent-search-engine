package com.j.web.eztorrent.entity;

import java.util.Arrays;

/**
 * Created by lavalamp on 30/03/2017.
 */
public enum Source {
    PIRATE_BAY(0),
    CACHE(9999);

    private int val;

    private Source(int val) {
        this.val = val;
    }

    public static Source find(int val) {
        return Arrays.stream(Source.values()).filter(source -> source.val == val).findFirst().orElse(PIRATE_BAY);
    }

    public int val() {
        return val;
    }

}
