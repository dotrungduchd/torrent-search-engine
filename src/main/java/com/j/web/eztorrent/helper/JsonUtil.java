/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.j.web.eztorrent.helper;

import com.google.gson.Gson;
import com.j.web.eztorrent.entity.MovieInfo;
import com.j.web.eztorrent.entity.ResultInfo;

/**
 *
 * @author dotrungduchd
 */
public class JsonUtil {

    private static Gson gson = new Gson();
    
    public String serialize(MovieInfo aThis) {
        return gson.toJson(aThis);
    }
    
    public String serialize(ResultInfo aThis) {
        return gson.toJson(aThis);
    }
}
