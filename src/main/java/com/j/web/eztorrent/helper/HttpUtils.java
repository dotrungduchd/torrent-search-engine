/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.j.web.eztorrent.helper;

import com.torrent.search.engine.RARBGSearchEngine;
import com.torrent.search.engine.constants.TorrentConstants;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

/**
 *
 * @author dotrungduchd
 */
public class HttpUtils {

    public static Document getDocumentByJsoup(String url) {
        Document doc = null;
        try {
            doc = Jsoup.connect(url)
                    .userAgent(TorrentConstants.USER_AGENT)
                    .header("Host", "rarbg.to")
                    .header("User-Agent", TorrentConstants.USER_AGENT)
                    .header("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")
                    .header("Accept-Language", "en-US,en;q=0.8")
                    .header("Accept-Encoding", "gzip, deflate, br")
                    .header("Content-Type", "application/x-www-form-urlencoded")
                    .header("X-Requested-With", "XMLHttpRequest")
                    .header("Cookie", TorrentConstants.COOKIES)
                    .header("Cache-Control", "max-age=0")
                    .header("Connection", "keep-alive")
                    .header("Upgrade-Insecure-Requests", "1")
                    .get();
        } catch (IOException ex) {
            Logger.getLogger(RARBGSearchEngine.class.getName()).log(Level.SEVERE, null, ex);
        }

        return doc;
    }

    public Map<String, String> parseCookie(String cookies) {

        Map<String, String> map = new HashMap<>();

        String[] pair = cookies.split(";");
        for (String pairString : pair) {
            String[] str = pairString.split("=");
            String value = str.length == 1 ? "" : str[1];
            map.put(str[0], value);
        }

        return map;
    }

    public static String getHtml(String url) throws MalformedURLException, IOException {
        String content = null;
        try {
            HttpURLConnection httpcon = (HttpURLConnection) new URL(url).openConnection();
            httpcon.addRequestProperty("User-Agent", TorrentConstants.USER_AGENT);
            Scanner scanner = new Scanner(httpcon.getInputStream());
            scanner.useDelimiter("\\Z");
            content = scanner.next();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        System.out.println(content);
        return content;
    }

}
