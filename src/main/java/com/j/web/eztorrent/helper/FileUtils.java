/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.j.web.eztorrent.helper;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 *
 * @author dotrungduchd
 */
public class FileUtils {

    public static void write(String fileName, String content) {

        try (BufferedWriter bw = new BufferedWriter(new FileWriter(fileName))) {

            bw.write(content);

            System.out.println("Done");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static String read(String fileName) {

        String content = "";
        try (BufferedReader bw = new BufferedReader(new FileReader(fileName))) {

            while (bw.readLine() != null) {
                content += bw.readLine();
            }

            System.out.println("Done");
        } catch (IOException e) {
            e.printStackTrace();
        }

        return content;
    }
}
